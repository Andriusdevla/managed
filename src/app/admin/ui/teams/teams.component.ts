import { Component, OnInit, Input } from '@angular/core';
import { ProjectService } from 'src/app/back/project.service';
import { UploadFileService } from 'src/app/back/upload/upload-file.service';
import { map } from 'rxjs/operators';
import { FileUpload } from '../../../back/upload/fileupload'
import { Teams } from './teams'
import {AuthService} from "../../../auth/auth.service";
import {AngularFireDatabase} from "@angular/fire/database";
import { Member } from './member';
import { Tasks } from './task';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import {AngularFireAuth} from "@angular/fire/auth";

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss']
})
export class TeamsComponent implements OnInit {
  team = {} as Teams;
  tasks = {} as Tasks;
  member = {} as Member;


  allTeams;
  constructor(public uploadService: UploadFileService, public projectService: ProjectService, public afAuth: AngularFireAuth,
    private db: AngularFireDatabase, private auth: AuthService) { 
  this.getTeamas();
    }


    getTeamas() {
      this.db.list(`teamsAll/${this.auth.userId}/`).snapshotChanges().pipe(
        map(changes =>
          changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
        )
      ).subscribe((data) => 
      this.allTeams = data
        )
      }


  ngOnInit() {

  }


  saveTeam(){
    this.db.list(`teamsAll/${this.auth.userId}/`).push(this.team);
  }
  
  //--get Team


  saveMember(item){
    this.db.list(`teamsAll/${this.auth.userId}/${item.key}/members`).push(this.member);
  }
  allMembers;
  //--get Team
  getMembers(){
  this.db.list(`teamsAll/${this.auth.userId}/`).snapshotChanges().pipe(
    map(changes =>
      changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
    )
  ).subscribe((data) => 
  console.log('Teams', data)
    )
  }
}
