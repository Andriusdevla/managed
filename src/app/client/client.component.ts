import { Component, OnInit, OnDestroy } from '@angular/core';
import {AngularFireDatabase} from "@angular/fire/database";
import { map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { Mes } from './../ui/admin/inner/mes'
@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {
  routeData;
  project;
  key;
  data;
  team;
  tasks;
  info;
  images;
  teams;
  msgs = {} as Mes;
  constructor(private db: AngularFireDatabase, private route: ActivatedRoute) {
    this.routeData = this.route.params.subscribe(params => {
      this.key = params['key']; // (+) converts string 'id' to a number
   });
   console.log(this.key)
   this.getAllProjects();
   this.getTeams();
   this.getKey();
   }

  ngOnInit() {
  }


  getTeams(){
    this.db.list(`${this.key}/2`).snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    ).subscribe((data) => {
     this.tasks = data;
    //  for(let task of data){
    //   this.tasks = task.tasks;
    //   console.log(this.tasks)
    //  }
    }  )}




  getAllProjects(){
    this.data = this.db.list(`${this.key}`).snapshotChanges().pipe(
       map(changes =>
         changes.map(c => ({ key: c.payload.key,...c.payload.val() }))
       )
     ).subscribe((data:any)  => {
      this.project = data;
     }
       )
   }

  objectValues(obj) { 
if(obj){
  return Object.values(obj || {});
}
 
}

ngOnDestroy(){
  this.data.unsubscribe();
}




getTeamPoints(team) {
if(team){
  return (<any>Object).values(team.tasks).reduce((total, entry) =>  total + entry.pt, 0);
}
}

getProjectTeamsTotalPoints(teams): number {
  return teams.reduce((total, team) => {
      if (!team || !team.tasks) {
          return total;
      }
      return (<any>Object).values(team.tasks).reduce((pts, task) => {
          return pts + task.pt;
      }, total);
  }, 0);
}

getDonePoints(teams): number {
  return teams.reduce((total, team) => {
      if (!team || !team.tasks) {
          return total;
      }
      return (<any>Object).values(team.tasks).reduce((pts, task) => {
          return pts + task.done;
      }, total);
  }, 0);
}


getDone(team){
  return (<any>Object).values(team.tasks).reduce((total, entry) =>  total + entry.done, 0);
}

chatBox = false;
chat(){
  this.chatBox = !this.chatBox;
}

sendMsg(){
 this.msgs.msg
this.msgs.from = 'Client',
console.log('ADMIN RAKTAS',this.adminKey);
  this.db.list(`projects/${this.adminKey}/${this.key}/info/msgs`).push(this.msgs)
  this.db.list(`${this.key}/1/msgs`).push(this.msgs)
}

msgArray;
adminKey;
getKey(){
 let sub = this.db.list(`${this.key}`).snapshotChanges().pipe(
    map(changes =>
      changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
    )
  ).subscribe((data) => {
    console.log(data)
   this.adminKey = data[1].key;
   console.log('Admin-key  values', this.adminKey)
  sub.unsubscribe();
  this.getMsg();


  }  )} 

getMsg(){
  //ADMIN
  this.db.list(`projects/${this.adminKey}/${this.key}/info/msgs`).snapshotChanges().pipe(
    map(changes =>
      changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
    )
  ).subscribe((data) => {
   this.msgArray = data;
   console.log('Gaunamas chagtas :', this.msgArray);
  }  )}
}
