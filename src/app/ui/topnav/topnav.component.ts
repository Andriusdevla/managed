import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../auth/auth.service";
import {AngularFireAuth} from "@angular/fire/auth";
import {Router} from "@angular/router";
import {AngularFireDatabase, AngularFireObject} from "@angular/fire/database";


export interface Profile {
  name: string;
  email: string;
}


@Component({
  selector: 'app-topnav',
  templateUrl: './topnav.component.html',
  styleUrls: ['./topnav.component.scss']
})

export class TopnavComponent implements OnInit {
  profile: AngularFireObject<Profile>
  logged = false;
  constructor(public auth: AuthService, public afAuth: AngularFireAuth,private router: Router,
    private afDatabase: AngularFireDatabase,) {
      this.getProfile();

  }

  ngOnInit() {
  }
  signOut() {
this.auth.signOut();
  }


  getProfile(){
    this.afAuth.authState.subscribe(data => {
      if(data && data.email && data.uid) {
        console.log(data.uid)
        this.profile = this.afDatabase.object(`Users/${data.uid}`)
        console.log("Gaunami profilio duomenys", this.profile)
        this.logged = true;
        console.log(this.logged)
      }
      else {
        console.log("Neprisijunges - profilis nematomas")
        this.logged = false;
        console.log(this.logged)
      }

    })
  }
}
