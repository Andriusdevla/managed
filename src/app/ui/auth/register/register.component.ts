import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {AuthService} from "../../../auth/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  passwordFormControl = new FormControl('', [
    Validators.required,
  ]);
  name = new FormControl('', [
    Validators.required,
  ]);
  repeatpasswordFormControl = new FormControl('', [
    Validators.required,
  ]);
  constructor(private auth: AuthService,
              public router: Router,) { }

  ngOnInit() {
  }

  signup() {
    //registracija

    let email = this.emailFormControl.value;
    let password = this.passwordFormControl.value;
    let name = this.name.value;
    this.auth.emailSignUp(email, password, name)
  }

}
