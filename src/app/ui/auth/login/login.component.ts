import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {Router} from "@angular/router";
import {RegisterComponent} from "../register/register.component";
import {AuthService} from "../../../auth/auth.service";
import {Observable} from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user: Observable<any>;
  email: string;
  emailSent = false;

  errorMessage: string;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  passwordFormControl = new FormControl('', [
    Validators.required,
  ]);
  constructor(private router: Router, public auth: AuthService, private afAuth : AngularFireAuth) { }

  ngOnInit() {
    this.user = this.afAuth.authState;

    const url = this.router.url;

    this.confirmSignIn(url);
  }

  async confirmSignIn(url){
    console.log('bg')
try {
  if(this.afAuth.auth.isSignInWithEmailLink(url)){
  let email = window.localStorage.getItem('emailForSignIn');
  console.log('try');
  if(!email){
    console.log('no-email');
    email = window.prompt('Please provide your email for confirmation')
  }
  const result = await this.afAuth.auth.isSignInWithEmailLink(url)
  console.log('result', result);
  window.localStorage.removeItem('emailForSignIn');
  }
}
catch(err) {
this.errorMessage = err.errorMessage;
console.log(this.errorMessage)
}
  }
  login(){

      this.auth.emailLogin(this.emailFormControl.value, this.passwordFormControl.value)

  }


}
