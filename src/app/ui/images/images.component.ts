import { Component, OnInit, Input } from '@angular/core';
import { FileUpload } from '../../back/upload/fileupload';
import { UploadFileService } from '../../back/upload/upload-file.service';
@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.scss']
})
export class ImagesComponent implements OnInit {
  @Input() fileUpload: FileUpload;
  constructor(private uploadService: UploadFileService) {
   }

  ngOnInit() {
  }

  deleteFileUpload(fileUpload) {
    this.uploadService.deleteFileUpload(fileUpload);
  }
}
