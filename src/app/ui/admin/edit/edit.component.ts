import { Component, OnInit, OnDestroy } from '@angular/core';
import {AngularFireDatabase} from "@angular/fire/database";
import { map } from 'rxjs/operators';
import {AuthService} from "../../../auth/auth.service";
import { ActivatedRoute } from '@angular/router';
import { UploadFileService } from 'src/app/back/upload/upload-file.service';
import { ProjectService } from 'src/app/back/project.service';
@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit,OnDestroy {
project;
data;
routeData;
key;
  constructor(private db: AngularFireDatabase,
    private auth: AuthService, private route: ActivatedRoute, public uploadService: UploadFileService,) {
      this.routeData = this.route.params.subscribe(params => {
        this.key = params['key']; // (+) converts string 'id' to a number
     });
     this.getAllProjects()
     }

  ngOnInit() {
  }

  
  getAllProjects(){
    this.data = this.db.list(`projects/${this.auth.userId}/${this.key}`).snapshotChanges().pipe(
       map(changes =>
         changes.map(c => ({ key: c.payload.key,...c.payload.val() }))
       )
     ).subscribe((data:any)  => {
       console.log('AAAAAAAAAAAAAA',data);
      this.project = data;
     }
       )
   }


   ngOnDestroy(){
     if(this.data){
      this.data.unsubscribe();
     }
    
   }
}
