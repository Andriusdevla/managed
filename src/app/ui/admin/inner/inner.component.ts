import { Component, OnInit, OnDestroy } from '@angular/core';
import {AngularFireDatabase} from "@angular/fire/database";
import { map } from 'rxjs/operators';
import {AuthService} from "../../../auth/auth.service";
import { ActivatedRoute } from '@angular/router';
import { forEach } from '@angular/router/src/utils/collection';
import { Mes } from './mes'
@Component({
  selector: 'app-inner',
  templateUrl: './inner.component.html',
  styleUrls: ['./inner.component.scss']
})
export class InnerComponent implements OnInit, OnDestroy {
 routeData;
project;
key;
data;
team;
tasks;
info;
images;
teams;
chatBox = false;

msgs = {} as Mes;
  constructor(  private db: AngularFireDatabase,
    private auth: AuthService, private route: ActivatedRoute) { 
      this.routeData = this.route.params.subscribe(params => {
        this.key = params['key']; // (+) converts string 'id' to a number
     });
     this.getAllProjects();
     this.getTeams();
     this.getMsg();
    }

  ngOnInit() {
  }

  getTeams(){
    this.db.list(`projects/${this.auth.userId}/${this.key}/teams`).snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    ).subscribe((data) => {
     this.tasks = data;
    //  for(let task of data){
    //   this.tasks = task.tasks;
    //   console.log(this.tasks)
    //  }
    }  )}




  getAllProjects(){
    this.data = this.db.list(`projects/${this.auth.userId}/${this.key}`).snapshotChanges().pipe(
       map(changes =>
         changes.map(c => ({ key: c.payload.key,...c.payload.val() }))
       )
     ).subscribe((data:any)  => {
      this.project = data;
     }
       )
   }

  objectValues(obj) { 
if(obj){
  return Object.values(obj || {});
}
 
}

ngOnDestroy(){
  this.data.unsubscribe();
}




getTeamPoints(team) {
if(team){
  return (<any>Object).values(team.tasks).reduce((total, entry) =>  total + entry.pt, 0);
}
}

getProjectTeamsTotalPoints(teams): number {
  return teams.reduce((total, team) => {
      if (!team || !team.tasks) {
          return total;
      }
      return (<any>Object).values(team.tasks).reduce((pts, task) => {
          return pts + task.pt;
      }, total);
  }, 0);
}

getDonePoints(teams): number {
  return teams.reduce((total, team) => {
      if (!team || !team.tasks) {
          return total;
      }
      return (<any>Object).values(team.tasks).reduce((pts, task) => {
          return pts + task.done;
      }, total);
  }, 0);
}


getDone(team){
  return (<any>Object).values(team.tasks).reduce((total, entry) =>  total + entry.done, 0);
}


chat(){
  this.chatBox = !this.chatBox;
}

sendMsg(){
 this.msgs.msg

this.msgs.from = this.project[1].name,
  this.db.list(`projects/${this.auth.userId}/${this.msgs.from}/info/msgs`).push(this.msgs)
  this.db.object(`${this.msgs.from}/1/key`).set(this.auth.userId);
  this.db.list(`${this.msgs.from}/1/msgs`).push(this.msgs)
}

msgArray;
getMsg(){

  //ADMIN
  this.db.list(`projects/${this.auth.userId}/${this.key}/info/msgs`).snapshotChanges().pipe(
    map(changes =>
      changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
    )
  ).subscribe((data) => {
   this.msgArray = data;
   console.log(this.msgArray);
  }  )}


  //CLIENT 

}
