import { Component, OnInit, Input } from '@angular/core';
import { ProjectService } from 'src/app/back/project.service';
import { UploadFileService } from 'src/app/back/upload/upload-file.service';
import { map } from 'rxjs/operators';
import { FileUpload } from '../../../back/upload/fileupload'
import { Project } from './pro'
import { Team } from './team'
import {AuthService} from "../../../auth/auth.service";
import {AngularFireDatabase} from "@angular/fire/database";
import { Member } from './member';
import { Tasks } from './tasks';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import {AngularFireAuth} from "@angular/fire/auth";
@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})

export class AddComponent implements OnInit {
  user: Observable<any>;
  emailSent = false;
  errorMessage: string;
 letall = false;

  error;
  //list of images :
  fileUploads: any[];
  updated = false;
  //upload image :
  selectedFiles: FileList;
  currentFileUpload: FileUpload;
  progress: { percentage: number } = { percentage: 0 };

  //upload info for project
  pro = {} as Project;
  team = {} as Team;
  tasks = {} as Tasks;
  member = {} as Member;
  //getting teams 
  teams: any;
  members: any;
  images: any;
  currentTasks = [];
  routeData;
  key;
  check;
  cname: string;
  constructor(public uploadService: UploadFileService, public projectService: ProjectService, public afAuth: AngularFireAuth,
    private db: AngularFireDatabase, private auth: AuthService, private router: Router, private route: ActivatedRoute) {
      console.log('erriras :', this.error)
      this.routeData = this.route.params.subscribe(params => {
        this.key = params['key']; // (+) converts string 'id' to a number
     });

     this.uploadService.pro.name = this.key;
     this.db.list(`projects/${this.auth.userId}/${this.uploadService.pro.name}/teams/`).snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    ).subscribe((data) => 
      this.teams = data
      )
   }
 //Get Images :
  ngOnInit() {
    this.uploadService.getFileUploads(6).snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    ).subscribe(fileUploads => {
      this.fileUploads = fileUploads;
    });
  }

  ngOnDestroy(){
    this.letall = false;
  }


//Upload Images : 
selectFile(event) {
  const file = event.target.files.item(0);

  if (file.type.match('image.*')) {
    this.selectedFiles = event.target.files;
  } else {
    alert('invalid format!');
  }
}

upload() {
  const file = this.selectedFiles.item(0);
  this.selectedFiles = undefined;

  this.currentFileUpload = new FileUpload(file);
  this.uploadService.pushFileToStorage(this.currentFileUpload, this.progress);
}
 


//----------------------Save information for project :

//---save project information
saveData(){
  this.uploadService.emailas = this.uploadService.pro.cName.substring(0, this.uploadService.pro.cName.lastIndexOf("@"));
  console.log('ADD COMP :', this.uploadService.emailas);
  let b = this.db.list(`projects/${this.auth.userId}/${this.uploadService.pro.name}`).snapshotChanges().pipe(
    map(changes =>
      changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
    )
  ).subscribe((data)  => {
    this.check = data;
    console.log(this.check);
    if(this.check.length === 0) {
      this.db.object(`projects/${this.auth.userId}/${this.uploadService.pro.name}/info`).set(this.uploadService.pro);
      console.log('pridėjome');
     // this.router.navigate(['/']);
      this.letall = true;
    }
    else {
      this.error = true;
      this.db.object(`${this.uploadService.pro.name}/`).set(data);
    }
  }
  )
  

}

async sendEmailLink() {
  console.log('Emailas emailas: ', this.uploadService.pro.cName);
  const actionCodeSettings = { 
    url: `https://managedrop-78316.firebaseapp.com/#/clientView/${this.uploadService.pro.name}`,
    handleCodeInApp: true
   }
  try {
    await this.afAuth.auth.sendSignInLinkToEmail(
      this.uploadService.pro.cName,
      actionCodeSettings
    );
    window.localStorage.setItem('emailForSignIn', this.uploadService.pro.cName);
    this.emailSent = true;
    console.log(this.emailSent);
  } catch (err) {
    this.errorMessage = err.message;
    console.log(err);
  }
}
//---save project team information
saveTeam(){
  this.db.list(`projects/${this.auth.userId}/${this.uploadService.pro.name}/teams/`).push(this.team);
  this.db.list(`${this.uploadService.pro.name}/3/teams/`).push(this.team);
}

//--get Team
getTeams(){
this.db.list(`projects/${this.auth.userId}/${this.uploadService.pro.name}/teams/`).snapshotChanges().pipe(
  map(changes =>
    changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
  )
).subscribe((data) => 
  this.teams = data
  )
}


//---Add member to team
saveMember(item){
this.db.list(`projects/${this.auth.userId}/${this.uploadService.pro.name}/teams/${item.key}/members/`).push(this.member);
this.db.list(`${this.uploadService.pro.name}/2/teams/${item.key}/members/`).push(this.member);
}

//---Get Teams member Array
getMembers(){
  this.db.list(`projects/${this.auth.userId}/${this.uploadService.pro.name}/teams/`).snapshotChanges().pipe(
    map(changes =>
      changes.map(c => ({ key: c.payload.key,...c.payload.val() }))
    )
  ).subscribe((data:any) => 
  console.log(data)
    )
}


//---convert obj to array of values
objectValues(obj) { 
  if(obj){
    return Object.values(obj); 
  }

}

//--Add Job to Team
addJob(item){
  this.tasks.done = 0;
  this.db.list(`projects/${this.auth.userId}/${this.uploadService.pro.name}/teams/${item.key}/tasks`).push(this.tasks);
  this.db.list(`${this.uploadService.pro.name}/teams/${item.key}/tasks`).push(this.tasks);
  }


  getTeamByClick(item){
  this.currentTasks = item;
  console.log(this.currentTasks)
  }


  saveAll(){
    this.saveData();
    this.sendEmailLink();
    this.updated = true;
  }

  update(){
    this.router.navigate(['/'])
  }

}
