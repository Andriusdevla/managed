import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Router } from '@angular/router';
import {AngularFireDatabase} from "@angular/fire/database";
import { map } from 'rxjs/operators';
import {AuthService} from "../../../auth/auth.service";
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Tasks } from '../add/tasks'
import { Pro } from './pro'
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
 projects:any;
 data;
 hover = false;


  constructor(private router: Router,
    private db: AngularFireDatabase,
    private auth: AuthService,
    public dialog: MatDialog) {

  this.getAllProjects();
  
   }

  ngOnInit() {
  }
 
  //---Get All projects
  allProjects:any;
  getAllProjects(){
   this.data = this.db.list(`projects/${this.auth.userId}/`).snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({ key: c.payload.key,...c.payload.val() }))
      )
    ).subscribe((data:any) => 
    this.projects = data
      )
  }

  //---convert obj to array of values
objectValues(obj) { 
  return Object.values(obj); 
}

goToAdd(){
  //test
  this.router.navigate(['/add'])
}

goToInner(item){
    this.router.navigate(['/project', item.key]);

}

goToEdit(item){
  console.log('This worked')
  this.router.navigate(['/client', item.key]);
}

ngOnDestroy(){
this.data.unsubscribe();
}

openDialog(item): void {
  const dialogRef = this.dialog.open(EditModel, {
    width: '500px',
    data: { projectKey: item.key }
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log('The dialog was closed');
  });
}
}
export interface Key {
  projectKey: string;
}

@Component({
selector: 'edit-model',
templateUrl: 'edit.html',
styleUrls: ['./dashboard.component.scss']
})


export class EditModel{
  project;
  modelData;
  teams;
  projectName;
  key = {} as Key;
  teamData;
  tasks;
  done:number;
  taskArray = [];
constructor(
  public dialogRef: MatDialogRef<EditModel>,
  @Inject(MAT_DIALOG_DATA) public data : Key,
  private db: AngularFireDatabase,
  private auth: AuthService,
  
  ) {
  this.projectName = data.projectKey;
  this.getAllProject();
  this.getTeams();
  }

  getAllProject(){
    this.modelData = this.db.list(`projects/${this.auth.userId}/${this.projectName}`).snapshotChanges().pipe(
       map(changes =>
         changes.map(c => ({ key: c.payload.key,...c.payload.val() }))
       )
     ).subscribe((data:any) => {
      this.project = data;
      console.log(this.project);
     }
   
       )
   }

   getTeams(){
  this.teamData = this.db.list(`projects/${this.auth.userId}/${this.projectName}/teams`).snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    ).subscribe((data) => {
  this.teams = data;
     console.log(data);

     
    //  for(let task of data){
    //   this.tasks = task.tasks;
    //   console.log(this.tasks)
    //  }
    }  )}


onNoClick(): void {
  this.dialogRef.close();
}


objectValues(obj) { 
  if(obj){
    return Object.values(obj || {});
  }
   
  }

  taskValues(obj){
    this.taskArray = Object.values(obj || {});
    let b = Object.keys(obj || {});

    for(let entry of this.taskArray){
      for(let blex of b){
        console.log(blex);
        let result = Object.assign(entry,blex);
        console.log(result);
      }
    }
    
    



  }

  ngOnDestroy(){
    this.modelData.unsubscribe();
    this.teamData.unsubscribe();
  }



  getTeamPoints(team) {
    if(team){
      return (<any>Object).values(team.tasks).reduce((total, entry) =>  total + entry.pt, 0);
    }
    }
    
    getProjectTeamsTotalPoints(teams): number {
      return teams.reduce((total, team) => {
          if (!team || !team.tasks) {
              return total;
          }
          return (<any>Object).values(team.tasks).reduce((pts, task) => {
              return pts + task.pt;
          }, total);
      }, 0);
    }
    
    getDonePoints(teams): number {
      return teams.reduce((total, team) => {
          if (!team || !team.tasks) {
              return total;
          }
          return (<any>Object).values(team.tasks).reduce((pts, task) => {
              return pts + task.done;
          }, total);
      }, 0);
    }
    
    
    getDone(team){
      return (<any>Object).values(team.tasks).reduce((total, entry) =>  total + entry.done, 0);
    }

    
    deleteTeam(team){
   this.db.object(`projects/${this.auth.userId}/${this.projectName}/teams/${team.key}`).remove();
   this.db.object(`${this.projectName}/2/${team.key}`).remove();
    }

    deleteTask(task,team){
      this.db.object(`projects/${this.auth.userId}/${this.projectName}/teams/${team.key}/tasks/${task.key}`).remove();
      this.db.object(`${this.projectName}/2/${team.key}/tasks/${task.key}`).remove();
    }
    taskai = {} as Tasks;
    updateTask(task,team){
      console.log(task.key, team.key)
  this.taskai.done = task.value.pt;
  this.taskai.name = task.value.name;
  this.taskai.pt = task.value.pt;
  this.db.object(`projects/${this.auth.userId}/${this.projectName}/teams/${team.key}/tasks/${task.key}`).update(this.taskai)
  this.db.object(`${this.projectName}/2/${team.key}/tasks/${task.key}`).update(this.taskai)
    }
    updated = {} as Pro;
    updateProject(){

      this.db.object(`projects/${this.auth.userId}/${this.projectName}/info`).update(this.updated);
      this.db.object(`${this.projectName}/1`).update(this.updated);
    }

    deleteProject(){
      this.db.object(`projects/${this.auth.userId}/${this.projectName}`).remove();
      this.db.object(`${this.projectName}`).remove();
    }

 
}