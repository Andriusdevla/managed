import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "./auth/auth.service";
import {AngularFireAuth} from "@angular/fire/auth";
import {animate, state, style, transition, trigger} from "@angular/animations";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})

export class AppComponent {
  title = 'manage';
  constructor(private router: Router, public auth: AuthService,) {

  }

}
