import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AngularFireAuthModule} from "@angular/fire/auth";
import {AngularFirestoreModule} from "@angular/fire/firestore";
import {NotifyService} from "../notify.service";
import {AuthService} from "../auth.service";
import {AngularFireDatabase} from "@angular/fire/database";

@NgModule({
  imports: [
    CommonModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
  ],
  declarations: [],
  providers: [AuthService, NotifyService, AngularFireDatabase],
})
export class CoreModule { }
