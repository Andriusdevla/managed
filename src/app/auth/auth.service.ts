import { Injectable } from '@angular/core';
import {Profile} from "./profile";
import {AngularFireAuth} from "@angular/fire/auth";
import {AngularFirestore, AngularFirestoreDocument} from "@angular/fire/firestore";
import {Router} from "@angular/router";
import {NotifyService} from "./notify.service";
import {AngularFireDatabase} from "@angular/fire/database";
import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { of } from "rxjs";

interface User {
  uid: string;
  email?: string | null;
  photoURL?: string;
  displayName?: string;
}

@Injectable({
  providedIn: 'root'
})



export class AuthService {
  user: Observable<User | null>;
  userId: string;
  email: string;
  profile = {} as Profile;
  constructor(public afAuth: AngularFireAuth,
              private afs: AngularFirestore,
              private router: Router,
              private notify: NotifyService,
              private afDatabase: AngularFireDatabase,) {

    this.user = this.afAuth.authState.pipe(switchMap((user) => {
        if (user) {
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      }));

    this.afAuth.authState.subscribe(user => {
      if(user) {
        console.log(user.uid, user.email, user.displayName)
        this.userId = user.uid;
        this.email = user.email;
      }
    })
  }



  emailSignUp(email: string, password: string, name: string) {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
      .then((credentiel) => {
        this.notify.update('Welcome', 'success');
        this.router.navigate(['/']);
        return this.updateUserData(credentiel.user)
      })
      .catch((error) => this.handleError(error) );
  }

  emailLogin(email: string, password: string) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then((user:any) => {
        this.notify.update('Welcome!!!', 'success')
        this.router.navigate(['/']);
       // return this.updateUserData(user); // if using firestore

      })
      .catch((error) => this.handleError(error) );
  }
  signOut() {
    this.afAuth.auth.signOut().then(() => {
      this.router.navigate(['/login']);
     this.userId = undefined;
    });
  }

  private handleError(error: Error) {
    console.error(error);
    this.notify.update(error.message, 'error');
  }

 

  private updateUserData(user: User) {

    const userRef: AngularFirestoreDocument<User> = this.afs.doc(`users/${user.uid}`);

    const data: User = {
      uid: user.uid,
      email: user.email || null,
      displayName: user.displayName || 'nameless user',
      photoURL: user.photoURL || 'https://goo.gl/Fz9nrQ',
    };
    return userRef.set(data);
  }


  createProfile(data){
    this.afAuth.authState.subscribe(auth => {
      this.afDatabase.object(`Users/${auth && auth.email && auth.uid}`).set(data)
        .then(() => this.router.navigateByUrl("/"));
    })
  }

}
