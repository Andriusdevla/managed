import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MaterialModule} from "./material/material.module";
import { RegisterComponent } from './ui/auth/register/register.component';
import {LoginComponent} from "./ui/auth/login/login.component";
import { TopnavComponent } from './ui/topnav/topnav.component';
import { FormsModule, ReactiveFormsModule} from "@angular/forms";
import { DashboardComponent } from './ui/admin/dashboard/dashboard.component';
import {AngularFireModule} from "@angular/fire";
import {environment} from "../environments/environment";
import {CoreModule} from "./auth/core/core.module";
import { AddComponent } from './ui/admin/add/add.component';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { ImagesComponent } from './ui/images/images.component';
import { InnerComponent } from './ui/admin/inner/inner.component';
import { EditComponent } from './ui/admin/edit/edit.component';
import { EditModel } from './ui/admin/dashboard/dashboard.component';
import { ClientComponent } from './client/client.component';
import { TeamsComponent } from './admin/ui/teams/teams.component'
export const firebaseConfig = environment.firebase;
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    TopnavComponent,
    DashboardComponent,
    AddComponent,
    ImagesComponent,
    InnerComponent,
    EditComponent,
    EditModel,
    ClientComponent,
    TeamsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    BrowserAnimationsModule,
    MaterialModule,
    CoreModule,
    AngularFireStorageModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [EditModel]
})
export class AppModule { }
