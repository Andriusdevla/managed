import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from "./ui/auth/login/login.component";
import {RegisterComponent} from "./ui/auth/register/register.component";
import {DashboardComponent} from "./ui/admin/dashboard/dashboard.component";
import {AuthGuardService} from "./auth/auth.guard.service"
import { AddComponent } from './ui/admin/add/add.component';
import { InnerComponent } from './ui/admin/inner/inner.component'
import { EditComponent } from './ui/admin/edit/edit.component'
import { ClientComponent } from './client/client.component';
import { TeamsComponent } from './admin/ui/teams/teams.component'
const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: '', component: DashboardComponent, canActivate: [AuthGuardService] },
  { path: 'add', component: AddComponent, canActivate: [AuthGuardService] },
  { path: 'add/:key', component: AddComponent, canActivate: [AuthGuardService] },
  { path: 'project/:key', component: InnerComponent, canActivate: [AuthGuardService] },
  { path: 'edit/:key', component: EditComponent, canActivate: [AuthGuardService] },
  { path: 'clientView/:key', component: ClientComponent },
  { path: 'teams', component: TeamsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule],
  providers: [AuthGuardService]
})
export class AppRoutingModule { }
