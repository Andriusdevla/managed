import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import * as firebase from 'firebase/app';
import 'firebase/storage';
import { Project } from '../../ui/admin/add/pro'
import { FileUpload } from './fileupload';
import { AuthService } from 'src/app/auth/auth.service';

export interface Project {
  name: string;
  sDate: Date;
  eDate: Date;
  cName: string;
  desc: string;
}

@Injectable({
  providedIn: 'root'
})
export class UploadFileService {
  pro = {} as Project;
  emailas: string;
  private basePath = `/projects/${this.auth.userId}/${this.pro.name}/images`;

  constructor(private db: AngularFireDatabase, private auth: AuthService) { }

  pushFileToStorage(fileUpload: FileUpload, progress: { percentage: number }) {
    const storageRef = firebase.storage().ref();
    const uploadTask = storageRef.child(`${this.basePath}/${fileUpload.file.name}`).put(fileUpload.file);

    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot) => {
        // in progress
        const snap = snapshot as firebase.storage.UploadTaskSnapshot;
        progress.percentage = Math.round((snap.bytesTransferred / snap.totalBytes) * 100);
      },
      (error) => {
        // fail
        console.log(error);
      },
      () => {
        // success
        uploadTask.snapshot.ref.getDownloadURL().then(downloadURL => {
          console.log('File available at', downloadURL);
          fileUpload.url = downloadURL;
          fileUpload.name = fileUpload.file.name;
          this.saveFileData(fileUpload);
        });
      }
    );
  }

  private saveFileData(fileUpload: FileUpload) {
    console.log('EMAILAS :', this.emailas);
    this.db.list(`/projects/${this.auth.userId}/${this.pro.name}/images`).push(fileUpload);
    console.log('Image uploadas', this.pro.cName);
    this.db.list(`${this.pro.name}/images`).push(fileUpload);
  }

  getFileUploads(numberItems): AngularFireList<FileUpload> {
    return this.db.list(`${this.basePath}`, ref =>
      ref.limitToLast(numberItems));
  }

  deleteFileUpload(fileUpload: FileUpload) {
    this.deleteFileDatabase(fileUpload.key)
      .then(() => {
        this.deleteFileStorage(fileUpload.name);
      })
      .catch(error => console.log(error));
      this.db.list(`${this.basePath}/${this.auth.userId}/${fileUpload.key}`).remove()
  }

  private deleteFileDatabase(key: string) {
    return this.db.list(`${this.basePath}/`).remove(key);
  }

  private deleteFileStorage(name: string) {
    const storageRef = firebase.storage().ref();
    storageRef.child(`${this.basePath}/${name}`).delete();
  }
}
