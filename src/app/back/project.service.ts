import { Injectable } from '@angular/core';
import {AngularFireDatabase} from "@angular/fire/database";
import {AuthService} from "../auth/auth.service";

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  //image upload
  private basePath = '/uploads';
  constructor(private db: AngularFireDatabase, private auth: AuthService) { }

}
